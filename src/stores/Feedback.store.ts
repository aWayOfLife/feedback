import { writable } from "svelte/store";
import type { Feedback } from "../types/Feedback";
import { BASE_URL } from "../constants/AppConstants";


const createFeedbackStore = (feedbacks: Feedback[]) => {
    const { subscribe, update, set } = writable<Feedback[]>(feedbacks);

    const getFeedbacks = async() => {
        const response = await fetch(`${BASE_URL}/feedbacks.json`)
        const data = await response.json();
        const feedbacks = [];
        data && Object.keys(data)?.forEach(key => {
         feedbacks.push({...data[key], id: key});
        })
        set(feedbacks);
    }
    
    const addFeedback = async(feedback: Feedback) => {
        const response = await fetch(`${BASE_URL}/feedbacks.json`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(feedback)
        })
        const data = await response.json()
        feedback.id = data.name;
        update(existingFeedbacks => [...existingFeedbacks, feedback]);
        return data;
    }
    
    const deleteFeedback = async(id: string) => {
        const response = await fetch(`${BASE_URL}/feedbacks/${id}.json`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
            }
        })
        const data = await response.json()
        update(feedbacks =>
            feedbacks.filter(feedback => feedback.id !== id)
        );
        return data;
    }
    
    
    return {
        subscribe,
        getFeedbacks:async () => await getFeedbacks(),
        addFeedback:async (feedback:Feedback) => await addFeedback(feedback),
        deleteFeedback:async (id: string) => await deleteFeedback(id)
    }
}

export const feedbackStore = createFeedbackStore([]);

